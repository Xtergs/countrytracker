﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParseCountriesFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = File.ReadAllLines(@"C:\Users\xterg\Desktop\new.txt");
            StringBuilder builder = new StringBuilder();
            foreach (var line in lines)
            {
                var code =  line.Substring(0, 2);
                string working = line.Substring(2).Trim();
                int index = working.IndexOf(' ');
                double lat;
                bool success = double.TryParse(working.Substring(0, index).Trim(), out lat);
                if (!success)
                    continue;
                working = working.Substring(index).Trim();
                index = working.IndexOf(' ');
                double lng = double.Parse(working.Substring(0, index).Trim());
                builder.AppendLine($"new Tuple<string, double, double>(\"{code}\", {lat}, {lng}),");
            }
            string result = builder.ToString();
            Console.ReadKey();
        }
    }
}
