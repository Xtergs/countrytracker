﻿using System;
using CountryTracker.Models;
using CountryTracker.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CountryTracker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewCountry : ContentPage, INavigateBack
    {
        public NewCountryViewModel viewModel { get; }
        public NewCountry()
        {
            viewModel = new NewCountryViewModel();
            InitializeComponent();
            BindingContext = viewModel;
        }

        public NewCountry(TripModel tripModel) : this()
        {
            viewModel.LoadFromStorage(tripModel.Id);
        }

        private void OnToMainPage(object sender, EventArgs eventArgs)
        {
            (sender as INavigateBack).ToMainPage -= OnToMainPage;
            //Navigation.PopAsync();
            ToMainPage?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler ToMainPage;

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (viewModel is INavigateBack)
            {
                (viewModel as INavigateBack).ToMainPage += OnToMainPage;
            }
            if (string.IsNullOrWhiteSpace(viewModel.TripType))
            {
                if (Busniness.IsSelected)
                    viewModel.TripType = Busniness.Data.ToString();
                else
                    viewModel.TripType = Holiday.Data.ToString();
            }
            else
            {
                if (viewModel.TripType == Busniness.Data.ToString())
                {
                    Busniness.IsSelected = true;
                    Holiday.IsSelected = false;
                }
                else
                {
                    Busniness.IsSelected = false;
                    Holiday.IsSelected = true;
                }
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (viewModel is INavigateBack)
            {
                (viewModel as INavigateBack).ToMainPage -= OnToMainPage;
            }
        }

        private void BusinessTap(object sender, EventArgs e)
        {
            Busniness.IsSelected = true;
            Holiday.IsSelected = false;
            viewModel.TripType = Busniness.Data.ToString();
        }

        private void HolidayTap(object sender, EventArgs e)
        {
            Busniness.IsSelected = false;
            Holiday.IsSelected = true;
            viewModel.TripType = Holiday.Data.ToString();
        }
    }
}