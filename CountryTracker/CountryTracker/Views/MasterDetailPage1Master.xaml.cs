﻿using System.Collections.ObjectModel;
using CountryTracker.Resources;
using PropertyChanged;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CountryTracker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterDetailPage1Master : ContentPage
    {
        public ListView ListView;

        public MasterDetailPage1Master()
        {
            InitializeComponent();

            BindingContext = new MasterDetailPage1MasterViewModel();
            ListView = MenuItemsListView;
        }

        [ImplementPropertyChanged]
        class MasterDetailPage1MasterViewModel
        {
            public ObservableCollection<MasterDetailPage1MenuItem> MenuItems { get; set; }

            public MasterDetailPage1MasterViewModel()
            {
                MenuItems = new ObservableCollection<MasterDetailPage1MenuItem>(new[]
                {
                    new MasterDetailPage1MenuItem { Id = 0, Title = AppResources.Home, TargetType = typeof(MainPage)},
                    new MasterDetailPage1MenuItem { Id = 1, Title = AppResources.History,TargetType = typeof(MainPage) },
                    new MasterDetailPage1MenuItem { Id = 2, Title = AppResources.Settings, TargetType = typeof(Settigns)},
                    new MasterDetailPage1MenuItem { Id = 3, Title = AppResources.GenerateNewCountry, TargetType = null},
                });
            }

            
        }
    }
}