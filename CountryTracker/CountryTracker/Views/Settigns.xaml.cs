﻿using System;
using System.Linq;
using CountryTracker.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CountryTracker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Settigns : ContentPage, INavigateBack
    {
        private SettingsViewModel viewModel;
        public Settigns()
        {
            try
            {
                InitializeComponent();
                viewModel = new SettingsViewModel();
                BindingContext = viewModel;
                viewModel.SelectCountryCodeFromStorage();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //public Settigns(string countryCode)
        //{
        //    InitializeComponent();
        //    viewModel = new SettingsViewModel();
        //    BindingContext = viewModel;
        //    viewModel.SelectCountryByCode(countryCode);
        //}

        private void SubmitButton_Clicked(object sender, EventArgs e)
        {
            if (viewModel.SelectedCountry == null || viewModel.SelectedCountry.Code == null)
            {
                ErrorLabel.IsVisible = true;
                return;
            }
            viewModel.SaveNewValueCommand.Execute(null);
            if (Navigation.NavigationStack.Last() is MasterDetailPage1)
                Navigation.PopToRootAsync(true);
            else if (Navigation.NavigationStack.Any(x=> x is MasterDetailPage1))
            {
                Navigation.PopAsync();
            }
            else
            {
                ToMainPage?.Invoke(this, EventArgs.Empty);
            }
            BindingContext = null;
            viewModel = null;
        }

        public event EventHandler ToMainPage;
    }
}