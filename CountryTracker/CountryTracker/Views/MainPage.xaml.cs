﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Threading.Tasks;
using CountryTracker.Models;
using CountryTracker.Services;
using CountryTracker.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace CountryTracker.Views
{
    public partial class MainPage : ContentPage
    {
        private MainViewModel ViewModel;
        public MainPage()
        {
            try
            {
                ViewModel = new MainViewModel();
                try
                {
                    InitializeComponent();

                    
                    //map.VisibleRegion.WithZoom(2);

                }
                catch (Exception nex)
                {
                    throw;
                }
                BindingContext = ViewModel;
                AddPins(ViewModel.Trips);
                
            }
            catch (Exception ex)
            {
                throw;
            }
            MapInicialization();
        }

        private async void MapInicialization()
        {
            await Task.Delay(2000);
            await map.MoveCamera(CameraUpdateFactory.NewPositionZoom(map.CameraPosition.Target, 2));
        }

        private void ViewModelOnEditTrip(object sender, TripModel tripModel)
        {
            Navigation.PushAsync(new NewCountry(tripModel));
        }

        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName == nameof(MainViewModel.Trips))
            {
                AddPins(ViewModel.Trips);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.PropertyChanged += ViewModelOnPropertyChanged;
            ViewModel.EditTrip += ViewModelOnEditTrip;
            
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            ViewModel.PropertyChanged -= ViewModelOnPropertyChanged;
            ViewModel.EditTrip -= ViewModelOnEditTrip;
        }

        private void AddPins(IList<TripModel> trips)
        {
            try
            {
                map.Pins.Clear();
                foreach (var trip in trips)
                {
                    var position = Country.GetLocation(trip.CountryCode);
                    map.Pins.Add(new Pin()
                    {
                        BindingContext = trip,
                        Tag = trip,
                        Label = new RegionInfo(trip.CountryCode).DisplayName,
                        Position = new Xamarin.Forms.GoogleMaps.Position(position.Item1, position.Item2),
                    });
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void MenuButton_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Settigns());
        }

        private void Button_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new NewCountry());
        }

        private void Map_OnSelectedPinChanged(object sender, SelectedPinChangedEventArgs e)
        {
            if (e.SelectedPin == null)
            {
                ViewModel.SelectedTrip = null;
            }
            else
            {
                ViewModel.SelectedTrip = (TripModel) e.SelectedPin.BindingContext;
            }
        }

        private void Map_OnCameraChanged(object sender, CameraChangedEventArgs e)
        {
            
        }
    }
}
