﻿using System;

namespace CountryTracker.Views
{
    public interface INavigateBack
    {
        event EventHandler ToMainPage;
    }
}
