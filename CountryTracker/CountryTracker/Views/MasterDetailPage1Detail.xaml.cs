﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CountryTracker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterDetailPage1Detail : ContentPage
    {
        public MasterDetailPage1Detail()
        {
            InitializeComponent();
        }
    }
}