﻿using System;
using System.Linq;
using CountryTracker.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CountryTracker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterDetailPage1 : MasterDetailPage
    {
        public MasterDetailPage1()
        {
            InitializeComponent();
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterDetailPage1MenuItem;
            if (item == null)
                return;
            if (item.TargetType == null)
            {
                string newCountryCode = new FakePhoneService().GetCurrentCountryCode();
                Global.HandleRest.HanleCountryCode(newCountryCode);
                IsPresented = false;
                return;
            }
            NavigateToPage(item);
            MasterPage.ListView.SelectedItem = null;
        }

        private MasterDetailPage1MenuItem showedPage;

        private void NavigateToPage(MasterDetailPage1MenuItem item)
        {
            if (showedPage == item)
                return;
            var page = (Page)Activator.CreateInstance(item.TargetType);
            page.Title = item.Title;

            if (page is INavigateBack)
            {
                (page as INavigateBack).ToMainPage += OnToMainPage;
            }

            Detail = new NavigationPage(page);
            IsPresented = false;
            showedPage = item;
        }

        public void NavigateToPage(Type type)
        {
            var item =
                MasterPage.ListView.ItemsSource.OfType<MasterDetailPage1MenuItem>().First(x => (x).TargetType == type);
            NavigateToPage(item);
        }

        public void NavigateToPage(Page page)
        {
            page.Title = page.Title;

            if (page is INavigateBack)
            {
                (page as INavigateBack).ToMainPage += OnToMainPage;
            }

            Detail = new NavigationPage(page);
            showedPage = null;
            IsPresented = false;
        }

        private void OnToMainPage(object sender, EventArgs eventArgs)
        {
            (sender as INavigateBack).ToMainPage -= OnToMainPage;
            NavigateToPage(typeof(MainPage));
        }
    }
}