﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace CountryTracker.Converters
{
    public class CountryCodeToCountryConverter : IValueConverter
    {
        public CountryCodeToCountryConverter()
        {
            
        }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;
            return new RegionInfo(value as string).DisplayName;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
