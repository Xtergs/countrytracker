﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace CountryTracker.Converters
{
    public class PhoneCallsTimeDoubleToStringConverter : IValueConverter
    {
        public PhoneCallsTimeDoubleToStringConverter()
        {
            
        }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return TimeSpan.FromMilliseconds((double) value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
