﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CountryTracker.DAL;
using CountryTracker.Resources;
using CountryTracker.Services;
using PropertyChanged;
using Xamarin.Forms;

namespace CountryTracker.ViewModels
{
    public class CountryCode
    {
        public string DisplayName { get; set; }
        public string Code { get; set; }
        public override string ToString()
        {
            return DisplayName;
        }
    }
    [ImplementPropertyChanged]
    public class SettingsViewModel
    {
        private Storage st;
        public List<CountryCode> Codes { get; }

        static SettingsViewModel()
        {
            
        }

        public SettingsViewModel()
        {
            List<Country> notRecognizedCountries = new List<Country>();
            Codes = Country.List.Select(c =>
            {
                try
                {
                    RegionInfo region = new RegionInfo(c.TwoLetterCode);
                    return new CountryCode()
                    {
                        Code = c.TwoLetterCode,
                        DisplayName = region.DisplayName,
                    };
                }
                catch (Exception ex)
                {
                    notRecognizedCountries.Add(c);
                    //throw;
                    return null;
                }
            }).Where(x => x != null).ToList();
            st = new Storage();
            SaveNewValueCommand = new Command(SaveNewValueExecute);
        }

        public void SelectCountryByCode(string countyCode)
        {
            if (countyCode == null)
                return;
            SelectedCountry = Codes.First(x => string.Equals(x.Code, countyCode, StringComparison.OrdinalIgnoreCase));
        }

        public void SelectCountryCodeFromStorage()
        {
            string current = st.GetCurrentCountry();
            SelectCountryByCode(current);
        }

        public CountryCode SelectedCountry { get; set; } = new CountryCode() {Code = null, DisplayName = AppResources.SelectAAcountry};

        public Command SaveNewValueCommand { get; }

        private async void SaveNewValueExecute()
        {
            if (SelectedCountry != null && SelectedCountry.Code != null)
            {
                await st.AddMarker(SelectedCountry.Code);
            }
            //var countryCode = st.GetCurrentCountry();
            //if (countryCode == null)
            //    return;
            //SelectCountryByCode(countryCode);
        }

    }
}
