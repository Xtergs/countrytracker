﻿using System;
using CountryTracker.DAL;
using CountryTracker.Views;
using PropertyChanged;
using Xamarin.Forms;

namespace CountryTracker.ViewModels
{
    [ImplementPropertyChanged]
    public class NewCountryViewModel : INavigateBack
    {
        private Storage st;
        public NewCountryViewModel()
        {
            st = new Storage();
            SubmitCommand = new Command(SubmitExecute);
        }
        public DateTime DateOfReturning { get; set; }
        public DateTime MinimumDate { get; } = DateTime.Now;
        public string Subject { get; set; }
        public string TripType { get; set; }

        public string CountryCode { get; set; }
        private string _tripId;
        public void LoadFromStorage(string tripId)
        {
            var trip = st.GetTripById(tripId);
            CountryCode = trip.CountryCode;
            DateOfReturning = trip.DateMustReturn.LocalDateTime;
            Subject = trip.Subject;
            TripType = trip.TripType;
            _tripId = trip.Id;
        }

        public Command SubmitCommand { get; }
        
        private void SubmitExecute()
        {
            if (_tripId != null)
                st.EditTripById(_tripId, Subject, DateOfReturning, TripType);
            else
                st.EditTrip(CountryCode, Subject, DateOfReturning, TripType);
            ToMainPage?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler ToMainPage;
    }
}
