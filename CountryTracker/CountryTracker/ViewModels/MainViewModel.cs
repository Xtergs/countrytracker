﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using CountryTracker.DAL;
using CountryTracker.Models;
using PropertyChanged;
using Xamarin.Forms;

namespace CountryTracker.ViewModels
{
    [ImplementPropertyChanged]
    public class MainViewModel : INotifyPropertyChanged
    {
        public event EventHandler<TripModel> EditTrip; 
        private Storage st;
        public MainViewModel()
        {
            st = new Storage();
            DeleteSelectedTripCommand = new Command(DeleteSelectedTripExecute);
            EditSelectedTripCommand = new Command(EditSelectedTripExecute);

            Trips = new ObservableCollection<TripModel>(st.GetAllEndedTrips());
        }
        public ObservableCollection<TripModel> Trips { get; set; }
        public TripModel SelectedTrip { get; set; }

        public Command DeleteSelectedTripCommand { get; }
        public Command EditSelectedTripCommand { get; }

        private void DeleteSelectedTripExecute()
        {
            st.RemoveCurrentTripByTripId(SelectedTrip.Id);
            Trips.Clear();
            foreach (var t in st.GetAllEndedTrips())
                Trips.Add(t);
            OnPropertyChanged(nameof(Trips));
            SelectedTrip = null;
        }

        private void EditSelectedTripExecute()
        {
            if (SelectedTrip == null)
                return;
            OnEditTrip(SelectedTrip);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void OnEditTrip(TripModel e)
        {
            EditTrip?.Invoke(this, e);
        }
    }
}
