﻿using System;
using Realms;

namespace CountryTracker.Models
{
    public class CountryModel : RealmObject
    {
        [PrimaryKey]
        public string CountryCodeIso { get; internal set; }
        public DateTimeOffset StartTrackingUtc { get; internal set; }
        public DateTimeOffset LastTimeTrackingUtc { get; set; }
        public double TotalTimeInCountryMin { get; set; }
    }

    public class CurrentCountry : RealmObject
    {
        [PrimaryKey]
        public string CountryCode { get; internal set; }
    }
}
