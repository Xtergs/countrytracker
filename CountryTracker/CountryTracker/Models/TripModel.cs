﻿using System;
using Realms;

namespace CountryTracker.Models
{
    public class TripModel : RealmObject
    {
        [PrimaryKey]
        public string Id { get; set; }
        public string CountryCode { get; set; }
        public DateTimeOffset DateOfGoing { get; set; }
        public DateTimeOffset DateMustReturn { get; set; }
        public DateTimeOffset LastTimeNotifiedUtc { get; set; }
        public string Subject { get; set; }
        public string TripType { get; set; }
        public DateTimeOffset DateTimeReturned { get; set; }
        public double PhoneCallsTime { get; set; }
    }
}
