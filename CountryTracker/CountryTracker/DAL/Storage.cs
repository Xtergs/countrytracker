﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CountryTracker.Models;
using Realms;

namespace CountryTracker.DAL
{
    public class Storage
    {
        public Storage()
        {

        }

        public async Task<bool> AddMarker(string countryCode)
        {
            try
            {
                countryCode = countryCode.ToUpper();
                var instance = Realm.GetInstance();
                var currentCountrie = instance.All<CurrentCountry>().ToList();
                if (currentCountrie == null || !currentCountrie.Any())
                {
                    await instance.WriteAsync(x =>
                    {
                        try
                        {
                            x.Add(new CountryModel()
                            {
                                CountryCodeIso = countryCode,
                                LastTimeTrackingUtc = DateTimeOffset.UtcNow,
                                StartTrackingUtc = DateTimeOffset.UtcNow,
                                TotalTimeInCountryMin = 0
                            });
                            x.Add(new CurrentCountry()
                            {
                                CountryCode = countryCode
                            });
                            x.Add(new TripModel()
                            {
                                CountryCode = countryCode,
                                DateMustReturn = DateTimeOffset.MaxValue,
                                DateTimeReturned = DateTimeOffset.MaxValue,
                                DateOfGoing = DateTimeOffset.UtcNow,
                                LastTimeNotifiedUtc = DateTimeOffset.UtcNow,
                                TripType = "Business",
                                Subject = "",
                                Id = Guid.NewGuid().ToString(),
                            });
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                    }).ConfigureAwait(false);
                    return true;
                }
                else
                {
                    string code = currentCountrie.First().CountryCode;
                    if (code == countryCode)
                    {
                        await instance.WriteAsync(x =>
                        {
                            try
                            {
                                var model = x.Find<CountryModel>(countryCode);
                                DateTimeOffset nowUtc = DateTimeOffset.UtcNow;
                                model.TotalTimeInCountryMin += (nowUtc - model.LastTimeTrackingUtc).TotalMinutes;
                                model.LastTimeTrackingUtc = nowUtc;
                            }
                            catch (Exception ex)
                            {
                                throw;
                            }
                        }).ConfigureAwait(false);
                        return false;
                    }
                    else
                    {
                        await instance.WriteAsync(x =>
                        {
                            try
                            {
                                var current = x.Find<CurrentCountry>(code);

                                var model = x.Find<CountryModel>(current.CountryCode);
                                DateTimeOffset nowUtc = DateTimeOffset.UtcNow;
                                model.TotalTimeInCountryMin += (nowUtc - model.LastTimeTrackingUtc).TotalMinutes;
                                model.LastTimeTrackingUtc = nowUtc;

                                current.CountryCode = countryCode;

                                model = x.Find<CountryModel>(countryCode);
                                if (model == null)
                                {
                                    model = new CountryModel()
                                    {
                                        CountryCodeIso = countryCode,
                                        LastTimeTrackingUtc = DateTimeOffset.UtcNow,
                                        StartTrackingUtc = DateTimeOffset.UtcNow,
                                        TotalTimeInCountryMin = 0
                                    };
                                    x.Add(model);
                                }
                                else
                                {
                                    model.LastTimeTrackingUtc = nowUtc;
                                }

                                var oldTrip = x
                                    .All<TripModel>()
                                    .FirstOrDefault(t => t.CountryCode == code && t.DateTimeReturned == DateTimeOffset.MaxValue);
                                if (oldTrip != null)
                                {
                                    oldTrip.DateTimeReturned = DateTimeOffset.UtcNow;
                                }
                                x.Add(new TripModel()
                                {
                                    CountryCode = countryCode,
                                    DateMustReturn = DateTimeOffset.MaxValue,
                                    DateTimeReturned = DateTimeOffset.MaxValue,
                                    DateOfGoing = DateTimeOffset.UtcNow,
                                    LastTimeNotifiedUtc = DateTimeOffset.UtcNow,
                                    TripType = "Business",
                                    Subject = "",
                                    Id = Guid.NewGuid().ToString(),
                                });

                            }
                            catch (Exception ex)
                            {
                                throw;
                            }
                        }).ConfigureAwait(false);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void EditTrip(string countryCode, string subject, DateTime? returning, string tripType)
        {
            var instance = Realm.GetInstance();
            var upperCountry = countryCode.ToUpper();
            var trip = instance
                .All<TripModel>()
                .Where(x => x.CountryCode == upperCountry)
                .OrderBy(x => x.DateOfGoing)
                .First();

            instance.Write(() =>
            {
                if (subject != null)
                    trip.Subject = subject;
                if (returning != null)
                    trip.DateMustReturn = new DateTimeOffset(returning.Value).ToUniversalTime();
                if (tripType != null)
                    trip.TripType = tripType;
            });

        }

        public TripModel[] GetAllEndedTrips()
        {
            var trips = Realm.GetInstance().All<TripModel>()
                //.Where(x => x.DateTimeReturned != DateTimeOffset.MaxValue)
                .OrderByDescending(x => x.DateTimeReturned).ToList()
                .GroupBy(x => x.CountryCode).Select(x => x.First()).ToArray();

            return trips;
        }

        public TripModel[] GetAllNotFinishedTrips()
        {
            var trips = Realm.GetInstance()
                .All<TripModel>()
                .Where(x => x.DateTimeReturned == DateTimeOffset.MaxValue)
                .OrderByDescending(x => x.DateTimeReturned)
                .ToList()
                .GroupBy(x => x.CountryCode)
                .Select(x => x.First()).ToArray();
            return trips;
        }

        public TimeSpan GetTotalTimeByCode(string countryCode)
        {
            var model = Realm.GetInstance().Find<CountryModel>(countryCode);
            return TimeSpan.FromMinutes(model.TotalTimeInCountryMin);
        }

        public string GetCurrentCountry()
        {
            var instance = Realm.GetInstance();
            var currentCountrie = instance.All<CurrentCountry>().ToList();
            if (currentCountrie == null || !currentCountrie.Any())
                return null;
            return currentCountrie.First().CountryCode;
        }

        public void RemoveCurrentTripByTripId(string tripId)
        {
            var instance = Realm.GetInstance();
            var toDelteTrip = instance.Find<TripModel>(tripId);
            instance.Write(() =>
            {
                instance.Remove(toDelteTrip);
            });
        }

        public TripModel GetTripById(string tripId)
        {
            return Realm.GetInstance().Find<TripModel>(tripId);
        }

        public void EditTripById(string tripId, string subject, DateTime? returning, string tripType)
        {
            var instance = Realm.GetInstance();
            var trip = instance.Find<TripModel>(tripId);

            instance.Write(() =>
            {
                if (subject != null)
                    trip.Subject = subject;
                if (returning != null)
                    trip.DateMustReturn = new DateTimeOffset(returning.Value).ToUniversalTime();
                if (tripType != null)
                    trip.TripType = tripType;
            });
        }
    }
}
