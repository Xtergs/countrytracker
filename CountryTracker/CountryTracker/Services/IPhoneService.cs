﻿using System;
using System.Globalization;

namespace CountryTracker.Services
{
    public interface IPhoneService
    {
        string GetCurrentCountryCode();
    }

    public class FakePhoneService : IPhoneService
    {
        private Random rand = new Random();
        public string GetCurrentCountryCode()
        {
            while (true)
            {
                string twoLetters = Country.List[rand.Next(0, Country.List.Length)].TwoLetterCode;
                try
                {
                    var val = new RegionInfo(twoLetters);
                    string dis = val.DisplayName;
                    return twoLetters;
                }
                catch { }
            }
        }
    }
}
