﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CountryTracker.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RadioButtonElement : ContentView
    {

        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(RadioButtonElement), "");

        public static readonly BindableProperty IsSelectedProperty = BindableProperty.Create(nameof(IsSelected),
            typeof(bool), typeof(RadioButtonElement), false);

        public static readonly BindableProperty DataProperty = BindableProperty.Create("Data", typeof(object),
            typeof(RadioButtonElement));

        public object Data
        {
            get { return (object) GetValue(DataProperty); }
            set { SetValue(DataProperty, value);}
        }

        public string Text
        {
            get { return (string) GetValue(TextProperty); }
            set { SetValue(TextProperty, value);}
        }

        public bool IsSelected
        {
            get { return (bool) GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value);}
        }
        public RadioButtonElement()
        {
            InitializeComponent();
            BindingContext = this;
        }
    }
}