﻿using System;
using System.Diagnostics;
using CountryTracker.DAL;
using CountryTracker.Resources;
using CountryTracker.Services;
using CountryTracker.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly:XamlCompilation(XamlCompilationOptions.Compile)]
namespace CountryTracker
{
    public partial class App : Application
    {
        public App()
        {
            try
            {
                if (Device.OS == TargetPlatform.iOS || Device.OS == TargetPlatform.Android)
                {
                    var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
                    AppResources.Culture = ci; 
                    DependencyService.Get<ILocalize>().SetLocale(ci); 
                }
                InitializeComponent();
                Storage storage = new Storage();
                var currentCode = storage.GetCurrentCountry();

                if (currentCode == null)
                {
                    var page = new MasterDetailPage1();
                    MainPage = page;
                    page.NavigateToPage(typeof(Settigns));
                }
                else
                {
                    var page = new MasterDetailPage1();
                    MainPage = page;
                    page.NavigateToPage(typeof(MainPage));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.StackTrace);
                throw;
            }

        }

        public void NavigateToNewCountryPage(string country)
        {
            var page = MainPage as MasterDetailPage1;
            var newpage = new NewCountry()
            {

            };
            newpage.viewModel.CountryCode = country.ToUpper();
            page.NavigateToPage(newpage);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
