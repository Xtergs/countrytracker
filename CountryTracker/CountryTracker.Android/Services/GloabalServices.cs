﻿using System;
using System.Diagnostics;
using System.Globalization;
using Android.App;
using Android.Content;
using CountryTracker.DAL;
using CountryTracker.Resources;
using CountryTracker.Services;

namespace CountryTracker.Droid.Services
{
    public class GloabalServices : IHandleReqest
    {
        public static IPhoneService PhoneService { get; set; }
        public static IAlarmServiceHelper AlarmService { get; set; }

        public static async void HandleCountryCode(string countrycode, Context context)
        {
            NotificationManager notificationManager = NotificationManager.FromContext(context);
            var builder = new Notification.Builder(context);
            var storage = new Storage();
            if (string.IsNullOrEmpty(countrycode))
                return;
            try
            {
                bool isNew = await storage.AddMarker(countrycode).ConfigureAwait(false);
                if (isNew)
                {
                    Intent newCountryIntent = new Intent(context, typeof(MainActivity));
                    newCountryIntent.PutExtra("newCountry", countrycode);
                    PendingIntent pendingIntent = PendingIntent.GetActivity(context, 1, newCountryIntent,
                        PendingIntentFlags.OneShot);
                    builder.SetContentTitle(AppResources.NewCountry).SetSmallIcon(Resource.Drawable.icon).SetContentText(
                            string.Format(AppResources.YouAreInNewCountrySetup, new RegionInfo(countrycode).DisplayName))
                        .SetContentIntent(pendingIntent);
                    notificationManager.Notify(0, builder.Build());
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.StackTrace);
                throw;
            }

        }

        public static async void ShowNotifictionForTrip(string tripId, Context context)
        {
            NotificationManager notificationManager = NotificationManager.FromContext(context);
            var builder = new Notification.Builder(context);
            var storage = new Storage();
            if (string.IsNullOrEmpty(tripId))
                return;
            try
            {
                var trip = storage.GetTripById(tripId);
                if (trip.DateTimeReturned != DateTimeOffset.MinValue || trip.DateMustReturn <= DateTimeOffset.UtcNow)
                    return;

                Intent newCountryIntent = new Intent(context, typeof(MainActivity));
                newCountryIntent.PutExtra("newCountry", trip.CountryCode);
                PendingIntent pendingIntent = PendingIntent.GetActivity(context, 1, newCountryIntent,
                    PendingIntentFlags.OneShot);
                builder.SetContentTitle(AppResources.TripIsEnding).SetSmallIcon(Resource.Drawable.icon).SetContentText(
                           String.Format(AppResources.TripEndingNotificationText, new RegionInfo(trip.CountryCode).DisplayName, (trip.DateMustReturn - DateTimeOffset.UtcNow).Days.ToString()))
                        .SetContentIntent(pendingIntent);
                    notificationManager.Notify(0, builder.Build());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.StackTrace);
                throw;
            }

        }

        public Context Context { get; set; }

        public void HanleCountryCode(string countryCode)
        {
            HandleCountryCode(countryCode, Context);
        }
    }
}