﻿using Android.Content;
using Android.Telephony;
using CountryTracker.Services;

namespace CountryTracker.Droid.Services
{
    
    class PhoneService : IPhoneService
    {
        private readonly Context _context;

        public PhoneService(Context context)
        {
            _context = context;
        }
        public string GetCurrentCountryCode()
        {
            TelephonyManager tm = (TelephonyManager.FromContext(_context));
            string countrycode = tm.NetworkCountryIso;
            return countrycode;
        }
    }

    
}