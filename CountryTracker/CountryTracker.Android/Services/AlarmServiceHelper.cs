﻿using System;
using Android.App;
using Android.Content;
using CountryTracker.DAL;
using Java.Util;
using Random = System.Random;

namespace CountryTracker.Droid.Services
{
    public interface IAlarmServiceHelper
    {
        void SetupAlarmService();
        bool SetupOneTimeNotification(DateTimeOffset alarmIn, string payload);
    }
    class AlarmServiceHelper : IAlarmServiceHelper
    {
        private readonly Context _context;

        public AlarmServiceHelper(Context context)
        {
            _context = context;
        }

        public void SetupAlarmService()
        {
            PendingIntent pendingIntent;
            Intent alarmIntent = new Intent(_context, typeof(BroadcastReceivers.AlarmReceiver));
            pendingIntent = PendingIntent.GetBroadcast(_context, 0, alarmIntent, PendingIntentFlags.UpdateCurrent);
            AlarmManager am = AlarmManager.FromContext(_context);

            Calendar c = Calendar.GetInstance(Java.Util.TimeZone.GetTimeZone("UTC"));
            c.TimeInMillis = Java.Lang.JavaSystem.CurrentTimeMillis();
            c.Add(CalendarField.Second, 5);
            am.SetInexactRepeating(AlarmType.Rtc, c.TimeInMillis, AlarmManager.IntervalHour,
                pendingIntent);

            Storage st = new Storage();
            var notFinishedTrips = st.GetAllNotFinishedTrips();
            foreach (var notFinishedTrip in notFinishedTrips)
            {
                SetupOneTimeNotification(notFinishedTrip.DateMustReturn.Subtract(TimeSpan.FromDays(15)),
                    notFinishedTrip.Id);
                SetupOneTimeNotification(notFinishedTrip.DateMustReturn.Subtract(TimeSpan.FromDays(1)),
                    notFinishedTrip.Id);
            }
        }

        public bool SetupOneTimeNotification(DateTimeOffset alarmIn, string payload)
        {
            PendingIntent pendingIntent;
            Intent alarmIntent = new Intent(_context, typeof(BroadcastReceivers.AlarmReceiver));
            alarmIntent = alarmIntent.PutExtra("remind", payload);
            var rand = new Random();
            pendingIntent = PendingIntent.GetBroadcast(_context, payload.GetHashCode(), alarmIntent, PendingIntentFlags.UpdateCurrent);
            AlarmManager am = AlarmManager.FromContext(_context);

            am.SetExact(AlarmType.Rtc, alarmIn.ToUnixTimeMilliseconds(), pendingIntent);

            return true;
        }
    }
}