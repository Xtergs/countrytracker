﻿using System.Threading.Tasks;
using Android.Content;
using Android.Telephony;
using CountryTracker.Droid.Services;

namespace CountryTracker.Droid.BroadcastReceivers
{
    [BroadcastReceiver(Enabled = true)]
    class AlarmReceiver : BroadcastReceiver
    {
        public override async void OnReceive(Context context, Intent intent)
        {
            await Task.Run(() =>
            {
                TelephonyManager tm = (TelephonyManager.FromContext(context));
                string countrycode = tm.NetworkCountryIso;
                GloabalServices.HandleCountryCode(countrycode, context);
            }).ConfigureAwait(false);
        }
    }
}