﻿using Android.App;
using Android.Content;
using CountryTracker.Droid.Services;

namespace CountryTracker.Droid.BroadcastReceivers
{
    [BroadcastReceiver]
    [IntentFilter(new [] {Intent.ActionBootCompleted})]
    class DeviceBootReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            var alaramService = new AlarmServiceHelper(context);
            alaramService.SetupAlarmService();
        }
    }
}