﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using AsNum.XFControls.Droid;
using CountryTracker.Droid.Services;
using CountryTracker.Services;
using HockeyApp.Android;
using Xamarin.Forms.Platform.Android;

namespace CountryTracker.Droid
{
    [Activity(Icon = "@drawable/icon", Theme = "@style/MainTheme", LaunchMode = LaunchMode.SingleTask, MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity :FormsAppCompatActivity
    {
        private App app;
        protected override void OnCreate(Bundle bundle)
        {
            
            base.OnCreate(bundle);

            AsNumAssemblyHelper.HoldAssembly();
            global::Xamarin.Forms.Forms.Init(this, bundle);
            Xamarin.FormsGoogleMaps.Init(this, bundle);
            app = new App();
            LoadApplication(app);

            GloabalServices.AlarmService = new AlarmServiceHelper(this);
            StartAlarmService();

            Global.HandleRest = new  GloabalServices() {Context = this};
        }

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            HandleIntent(intent);
        }

        private void StartAlarmService()
        {
            GloabalServices.AlarmService.SetupAlarmService();
        }

        protected override void OnResume()
        {
            base.OnResume();
            CrashManager.Register(this, "24c4678943f14feebbcbc0373ec072b6");
        }

        private void HandleIntent(Intent intent)
        {
            if (intent.HasExtra("newCountry"))
            {
                string countryCode = intent.GetStringExtra("newCountry");
                app.NavigateToNewCountryPage(countryCode);
            }
            if (intent.HasExtra("remind"))
            {
                string tripId = intent.GetStringExtra("remind");
                GloabalServices.ShowNotifictionForTrip(tripId, this);
            }
        }
    }
}

